import model.ParsingDOU;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class Main {

    public static void main(String[] args) throws IOException, InterruptedException {

        ParsingDOU.deleteAllFilesFolder(ParsingDOU.PATH);

        InputStream inputStream = Main.class.getClassLoader().getResourceAsStream("list_Job.csv");

        String result = IOUtils.toString(inputStream, StandardCharsets.UTF_8);

        String[] company = ParsingDOU.splitResult(result);

        Map<String, String> vacancy = ParsingDOU.splitCompany(company);

        ParsingDOU.downloadHtmlPage(vacancy);

        ParsingDOU.findVacancy();

    }


}
