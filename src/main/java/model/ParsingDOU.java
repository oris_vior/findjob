package model;

import com.sun.tools.javac.Main;
import lombok.SneakyThrows;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

public class ParsingDOU {
    public static final String DELIMETER = ",";
    public static final String CODE_NAME = "researcher";
    public static final boolean SWITCH_TYPE_OF_VIEW = false;
    public static final String PATH = "src/main/resources/site";

    public static void findVacancy() throws IOException {
        FileWriter writer = new FileWriter("src/main/resources/" + "vacancy" + ".html", false);
        writer.write("<!DOCTYPE HTML>\n" +
                "<html>\n" +
                "<head>\n" +
                "<style type=\"text/css\">\n" +
                "div {\n" +
                        "margin: 30px;\n" +
                        "}\n"+
                "a {\n" +
                "\tfont-family: Arial, Helvetica, sans-serif;\n" +
                "\tfont-size: 15px;\n" +
                "\tcolor: #fafafa;\n" +
                "\tpadding: 16px 26px;\n" +
                "\tdisplay: inline-block;\n" +
                "\ttext-decoration: none;\n" +
                "\tborder-radius: 3px;\n" +
                "\tbox-shadow: 0px 6px #27ae60;\n" +
                "\tbackground: #2ecc71;\n" +
                "}\n" +
                "a:hover {\n" +
                "\tbackground: #36d479;\n" +
                "}\n" +
                "a:active {\n" +
                "\tposition: relative;\n" +
                "\ttop: 6px;\n" +
                "\tbox-shadow: 0px 0px #23a33d;\n" +
                "\tbackground: #23a33d;\n" +
                "}\n" +
                " \n" +
                "</style>\n" +
                "</head>\n" +
                "<body>\n");


        for (File file : Objects.requireNonNull(new File(PATH).listFiles())) {

            Elements document = Jsoup.parse(file, "UTF-8", "hh.ru").select("a");
            for (Element element : document) {
                if (element.text().trim().toLowerCase().contains(CODE_NAME.toLowerCase())) {
                    if (SWITCH_TYPE_OF_VIEW) {
                        openWebpage(element.select("a").attr("href"));
                    } else {
                        writer.write("<div><a href=\"" + element.select("a").attr("href") + "\">" + element.text() + "</a></div>" + "\r\n");
                    }
                }
            }
        }


        writer.write("</body>\n" +
                "</html>");

        writer.flush();
        writer.close();
    }

    public static void deleteAllFilesFolder(String path) {
        for (File myFile : Objects.requireNonNull(new File(path).listFiles()))
            if (myFile.isFile()) myFile.delete();
    }

    public static void downloadHtmlPage(Map<String, String> vacancy) throws IOException {
        for (Map.Entry<String, String> entry : vacancy.entrySet()) {
            Document document = Jsoup.connect(entry.getValue()).get();

            if (document.getElementsByClass("vt").size() > 0) {
                FileWriter writer = new FileWriter("src/main/resources/site/" + entry.getKey() + ".html", false);
                writer.write(getElementsFromClass(document));
                writer.flush();
                writer.close();
            }
        }
    }

    private static String getElementsFromClass(Document document) {
        StringBuilder s = new StringBuilder();
        Elements vt = document.getElementsByClass("vt");
        for (Element element : vt) {
            s.append(element).append("\r\n");
        }
        return s.toString().replaceAll("&nbsp;", "");
    }

    public static void openWebpage(String urlString) {
        try {
            Desktop.getDesktop().browse(new URL(urlString).toURI());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Map<String, String> splitCompany(String[] company) {
        Map<String, String> nameAndSiteCompany = new LinkedHashMap<>();
        for (int i = 1; i < company.length; i++) {
            String[] propertiesCompany = ParsingDOU.splitLine(company[i]);
            if (checkLinkCompany(propertiesCompany[3])) {
                nameAndSiteCompany.put(propertiesCompany[0], propertiesCompany[3] + "vacancies/");
            }

        }
        return nameAndSiteCompany;
    }

    private static boolean checkLinkCompany(String s) {
        return s.startsWith("https");
    }

    @SneakyThrows
    public static String[] splitResult(String result) {
        return result.split("\\r\\n");
    }

    public static String[] splitLine(String line) {
        return line.split(ParsingDOU.DELIMETER);
    }
}
